# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_screen.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Chat(object):
    def setupUi(self, Chat):
        Chat.setObjectName("Chat")
        Chat.resize(360, 591)
        self.centralwidget = QtWidgets.QWidget(Chat)
        self.centralwidget.setObjectName("centralwidget")
        self.send = QtWidgets.QCommandLinkButton(self.centralwidget)
        self.send.setGeometry(QtCore.QRect(290, 520, 61, 30))
        self.send.setIconSize(QtCore.QSize(10, 10))
        self.send.setObjectName("send")
        self.editText = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.editText.setGeometry(QtCore.QRect(10, 500, 271, 51))
        self.editText.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.IBeamCursor))
        self.editText.setInputMethodHints(QtCore.Qt.ImhNone)
        self.editText.setObjectName("editText")
        self.chatText = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.chatText.setGeometry(QtCore.QRect(10, 80, 341, 401))
        self.chatText.setPlainText("")
        self.chatText.setObjectName("chatText")
        self.ipEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.ipEdit.setGeometry(QtCore.QRect(30, 20, 191, 31))
        self.ipEdit.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.IBeamCursor))
        self.ipEdit.setObjectName("ipEdit")
        self.portEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.portEdit.setGeometry(QtCore.QRect(270, 20, 81, 31))
        self.portEdit.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.IBeamCursor))
        self.portEdit.setObjectName("portEdit")
        Chat.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(Chat)
        self.statusbar.setObjectName("statusbar")
        Chat.setStatusBar(self.statusbar)
        self.menuBar = QtWidgets.QMenuBar(Chat)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 360, 23))
        self.menuBar.setObjectName("menuBar")
        self.menuFile = QtWidgets.QMenu(self.menuBar)
        self.menuFile.setObjectName("menuFile")
        self.menuEdit = QtWidgets.QMenu(self.menuBar)
        self.menuEdit.setObjectName("menuEdit")
        self.menuNetwork = QtWidgets.QMenu(self.menuBar)
        self.menuNetwork.setObjectName("menuNetwork")
        Chat.setMenuBar(self.menuBar)
        self.actionNetwork = QtWidgets.QAction(Chat)
        self.actionNetwork.setObjectName("actionNetwork")
        self.actionAbout = QtWidgets.QAction(Chat)
        self.actionAbout.setObjectName("actionAbout")
        self.actionExit = QtWidgets.QAction(Chat)
        self.actionExit.setObjectName("actionExit")
        self.actionConnect = QtWidgets.QAction(Chat)
        self.actionConnect.setObjectName("actionConnect")
        self.actionDisconnect = QtWidgets.QAction(Chat)
        self.actionDisconnect.setObjectName("actionDisconnect")
        self.actionPreferences = QtWidgets.QAction(Chat)
        self.actionPreferences.setObjectName("actionPreferences")
        self.menuFile.addAction(self.actionAbout)
        self.menuFile.addAction(self.actionExit)
        self.menuEdit.addAction(self.actionPreferences)
        self.menuNetwork.addAction(self.actionConnect)
        self.menuNetwork.addAction(self.actionDisconnect)
        self.menuBar.addAction(self.menuFile.menuAction())
        self.menuBar.addAction(self.menuEdit.menuAction())
        self.menuBar.addAction(self.menuNetwork.menuAction())

        self.retranslateUi(Chat)
        QtCore.QMetaObject.connectSlotsByName(Chat)

    def retranslateUi(self, Chat):
        _translate = QtCore.QCoreApplication.translate
        Chat.setWindowTitle(_translate("Chat", "MainWindow"))
        self.send.setText(_translate("Chat", "Send"))
        self.menuFile.setTitle(_translate("Chat", "File"))
        self.menuEdit.setTitle(_translate("Chat", "Edit"))
        self.menuNetwork.setTitle(_translate("Chat", "Network"))
        self.actionNetwork.setText(_translate("Chat", "Network"))
        self.actionAbout.setText(_translate("Chat", "About"))
        self.actionExit.setText(_translate("Chat", "Exit"))
        self.actionConnect.setText(_translate("Chat", "Connect"))
        self.actionDisconnect.setText(_translate("Chat", "Disconnect"))
        self.actionPreferences.setText(_translate("Chat", "Preferences"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Chat = QtWidgets.QMainWindow()
    ui = Ui_Chat()
    ui.setupUi(Chat)
    Chat.show()
    sys.exit(app.exec_())

