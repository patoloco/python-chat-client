import atexit, datetime, json, socket, sys, time

from PyQt5 import QtWidgets
from PyQt5.QtCore import QThread, pyqtSignal

from main_screen import Ui_Chat

from RepeatedTimer import RepeatedTimer

localhost_server_address = ("localhost", 3000)
command_prefix, message_prefix = "#CMD:", "#MSG:"

class Client(QtWidgets.QMainWindow):
    def __init__(self, ui):
        super().__init__()

        self.connected = False
        self.server_address = localhost_server_address
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.ui = ui

        # IP/Port Boxes
        self.ui.ipEdit.setPlainText(localhost_server_address[0])
        self.ui.ipEdit.textChanged.connect(self.setIP)
        self.ui.portEdit.setPlainText(str(localhost_server_address[1]))
        self.ui.portEdit.textChanged.connect(self.setPort)

        # Menu
        self.ui.actionExit.setShortcut("ALT+F4")
        self.ui.actionExit.setStatusTip("Exit the application")
        self.ui.actionExit.triggered.connect(self.close)

        self.ui.actionConnect.setShortcut("ALT+C")
        self.ui.actionConnect.triggered.connect(self.connect)

        self.ui.actionDisconnect.setShortcut("ALT+D")
        self.ui.actionDisconnect.triggered.connect(self.disconnect)

        # Buttons
        self.ui.send.clicked.connect(self.send_message)

    def setIP(self):
        if not self.connected == True:
            current_port = self.server_address[1]
            new_ip = self.ui.ipEdit.toPlainText()
            self.server_address = (new_ip, current_port)
        print(self.server_address)

    def setPort(self):
        if not self.connected == True:
            current_ip = self.server_address[0]
            new_port = int(self.ui.portEdit.toPlainText())
            self.server_address = (current_ip, new_port)
        print(self.server_address)

    def connect(self):
        if self.connected == True:
            print("already connected")
            return
        try:
            print("connecting to " + str(self.server_address))
            new_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            new_socket.connect(self.server_address)
            self.socket = new_socket

            if "timer" in self.__dict__:
                self.timer.stop()
            self.listener_thread = ListenerThread(self.socket)
            self.listener_thread.message_received.connect(self.msg_received)
            self.timer = RepeatedTimer(1, self.listener_thread.start)

            self.send_command("connect")
        except socket.error:
            print("failed to create socket")

    def disconnect(self):
        if not self.connected == True:
            print("not connected")
            return
        self.timer.stop()

        self.send_command("disconnect")
        self.add_message_to_chat_text("Disconnected", "SERVER")
        self.connected = False
        self.socket.close()

    def send_command(self, c):
        if not self.connected == True and not c == "connect":
            print("not connected")
            return
        cmd = command_prefix + c
        self.socket.sendto(cmd.encode("utf-8"), self.server_address)

    def send_message(self):
        if not self.connected == True:
            print("not connected")
            return
        elif self.ui.editText.toPlainText() == "":
            return

        msg = message_prefix + self.ui.editText.toPlainText()
        self.socket.sendto(msg.encode("utf-8"), self.server_address)
        self.ui.editText.setPlainText("")

    def msg_received(self, msg):
        if "message" in msg and "fromIp" in msg and "timestamp" in msg:
            self.add_message_to_chat_text(msg["message"], msg["fromIp"], msg["timestamp"])
        elif "response" in msg:
            response = msg["response"]
            print("response:" + str(response))

            if "message" in response:
                self.add_message_to_chat_text(response["message"], "Me")
            elif "command" in response:
                if response["command"] == "connect":
                    self.connected = True
                    self.add_message_to_chat_text("Connected", "SERVER")

    def add_message_to_chat_text(self, msg, fromIp, ts = None):
        if ts is None:
            ts = int(round(time.time()))  # seconds
        else:
            ts = int(round(ts / 1000))  # comes as ms from server

        # formatting
        time_string = datetime.datetime.fromtimestamp(ts)
        time_string = time_string.strftime("%H:%M:%S")

        tmp = self.ui.chatText.toPlainText()
        tmp += "\n" + fromIp + "(" + str(time_string) + ")" + ": " + str(msg)
        self.ui.chatText.setPlainText(tmp)

class ListenerThread(QThread):
    message_received = pyqtSignal(object)

    def __init__(self, socket):
        super().__init__()
        self.socket = socket

    def run(self):
        data = self.socket.recv(4096)
        msg = data.decode("utf-8")
        msg_json = json.loads(msg)
        if not msg == "":
            self.message_received.emit(msg_json)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_Chat()
    ui.setupUi(MainWindow)

    try:
        client = Client(ui)
    except:
        print("Error creating client")
        sys.exit(app.exec())

    atexit.register(client.send_command, "disconnect")
    MainWindow.show()
    sys.exit(app.exec_())
